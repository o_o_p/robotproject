/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattraporn.robotpj;

import java.util.Scanner;

/**
 *
 * @author Pattrapon N
 */
public class mainPG {

    public static void main(String[] args) {
        Scanner mk = new Scanner(System.in);
        TableMap map = new TableMap(10, 10);
        Robot robot = new Robot(2, 2, 'x', map);
        Bomb bomb = new Bomb(5, 5);
        map.setRobot(robot);
        map.setBomb(bomb);
        while (true) {
            map.showMap();
            char direction = inputDirection(mk);
            if(direction == 'q'){
                Bye();
                break;
            }
            robot.walk(direction);
        }

    }

    private static void Bye() {
        System.out.println("Bye Bye!");
    }

    private static char inputDirection(Scanner mk) {
        String str = mk.next();
        char direction = str.charAt(0);
        return direction;
    }
}
