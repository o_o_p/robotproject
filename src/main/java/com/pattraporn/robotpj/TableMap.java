/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattraporn.robotpj;

/**
 *
 * @author Pattrapon N
 */
public class TableMap {

    private int width;
    private int height;
    private Robot robot;
    private Bomb bomb;

    public TableMap(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void setRobot(Robot robot) {
        this.robot = robot;
    }

    public void setBomb(Bomb bomb) {
        this.bomb = bomb;
    }

    public void showMap() {
        showtitle();
        for (int y = 0; y < height; y++) {

            for (int x = 0; x < width; x++) {
                if (robot.isOn(x, y)) {
                    showRobot();
                } else if (bomb.isOn(x, y)) {
                    showbomb();
                } else {
                    map();
                }

            }
            shownewline();
        }

    }

    private void showtitle() {
        System.out.println("MAP");
    }

    private void shownewline() {
        System.out.println("-");
    }

    private void showbomb() {
        System.out.print(bomb.getSymbol());
    }

    private void map() {
        System.out.print("-");
    }

    private void showRobot() {
        System.out.print(robot.getSymbol());
    }

    public boolean inMap(int x, int y) {

        return (x >= 0 && x < width) && (y >= 0 && y < height);
    }

    public boolean isBomb(int x, int y) {
        return bomb.isOn(x, y);
    }
}
